<?php

namespace App\Tests;

use App\Entity\ToDoList;
use App\Entity\Item;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ToDoListTest extends KernelTestCase
{
    private Item $item;
    private ToDoList $todolist;

    public function setUp(): void
    {
        $this->todolist = new ToDoList();
        $this->todolist->setId(1);
    }

    public function testAddItem(): void
    {
        $item = new Item();
        $item->setName('itemTest');
        $item->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
        $item->setCreatedAt(new \DateTimeImmutable('now'));
        $item->setTodolistId($this->todolist->getId());

        $this->assertTrue($this->todolist->add($item));
    }

    public function testAddMaxItems(): void
    {
        $item = new Item();
        $item->setName('itemTest');
        $item->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
        $item->setCreatedAt(new \DateTimeImmutable('now'));
        $item->setTodolistId($this->todolist->getId());

        for ($i = 0; $i < 10; $i++) {
            $item->setName('itemTest' . $i);
            $newDate = $item->getCreatedAt()->add(new \DateInterval('PT45M'));
            $item->setCreatedAt($newDate);

            $insert = $this->todolist->add($item);
            if($i === 8) {
                $this->assertEquals('email sended', $insert);
            } else {
                $this->assertTrue($insert);
            }
        }

        $this->assertEquals(10, count($this->todolist->getItems()));
    }

    public function testAdd11Items(): void
    {
        $item = new Item();
        $item->setName('itemTest');
        $item->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
        $item->setCreatedAt(new \DateTimeImmutable('now'));
        $item->setTodolistId($this->todolist->getId());

        for ($i = 0; $i < 11; $i++) {
            $item->setName('itemTest' . $i);
            $newDate = $item->getCreatedAt()->add(new \DateInterval('PT45M'));
            $item->setCreatedAt($newDate);

            $insert = $this->todolist->add($item);
            if($i === 10) {
                $this->assertFalse($insert);
            } elseif($i === 8) {
                $this->assertEquals('email sended', $insert);
            } else {
                $this->assertTrue($insert);
            }
        }

        $this->assertEquals(10, count($this->todolist->getItems()));
    }
}