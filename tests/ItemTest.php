<?php

namespace App\Tests;

use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use App\Entity\Item;

class ItemTest extends TestCase
{
    private Item $item;

    public function setUp(): void
    {
        $this->item = new Item();
        $this->item->setName('itemTest');
        $this->item->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
        $this->item->setCreatedAt(new \DateTimeImmutable('now'));
        $this->item->setTodolistId(1);

        parent::setUp();
    }

    /**
     * Check if global is valid
     */
    public function testIsValid(): void
    {
        $this->assertTrue($this->item->isValid());
    }

    /**
     * Check if name is not empty
     */
    public function testIsNameInvalid(): void
    {
        $this->item->setName('');
        $this->assertFalse($this->item->isNameValid());
    }

    /**
     * Check if content is not empty
     */
    public function testIsContentEmptyInvalid(): void
    {
        $this->item->setContent('');
        $this->assertFalse($this->item->isContentValid());
    }

    /**
     * Check if content size > 1000
     */
    public function testIsContentSizeInvalid(): void
    {
        // 3031 characters
        $this->item->setContent(
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore 
                    et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                    nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                    dolore eu fugiat nulla pariatur.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.Lorem ipsum dolor 
                    sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                    dolore eu fugiat nulla pariatur.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.Lorem ipsum
                    dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
                    commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
                    voluptate velit esse cillum dolore eu fugiat nulla pariatur.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                    dolore eu fugiat nulla pariatur.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exe
                    rcitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur.
        ');
        $this->assertFalse($this->item->isContentValid());
    }

    /**
     * Check if date is not null
     */
    public function testIsDateValid(): void
    {
        $this->item->setCreatedAt(null);
        $this->assertFalse($this->item->isDateValid());
    }

    public function testDateInThePast(): void
    {
        $this->item->setCreatedAt(
            (new \DateTimeImmutable())->modify('-1 day')
        );
        $this->assertFalse($this->item->isDateValid());
    }

    public function testDateInTheFuture(): void
    {
        $this->item->setCreatedAt(
            (new \DateTimeImmutable())->modify('+1 day')
        );
        $this->assertTrue($this->item->isDateValid());
    }

    public function testDateNow(): void
    {
        $this->item->setCreatedAt(
            (new \DateTimeImmutable())
        );
        $this->assertTrue($this->item->isDateValid());
    }
}